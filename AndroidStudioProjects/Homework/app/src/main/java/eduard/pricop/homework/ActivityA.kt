package eduard.pricop.homework

import android.content.ContentValues.TAG
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_a.*
import kotlinx.android.synthetic.main.activity_b.*
import kotlinx.android.synthetic.main.activity_c.*


class ActivityA : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i(TAG, "onCreate: works!")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_a)

        buttonA.setOnClickListener {
            val intent : Intent = Intent(this, ActivityB::class.java)
            startActivity(intent)
        }

        buttonWeb.setOnClickListener{
            val intent1 : Intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://youtu.be/4UtBQfs8F9I"))
            startActivity(intent1)
        }

        button4.setOnClickListener {
            val intent2 :Intent = Intent(this, Callback::class.java)
            startActivity(intent2)
        }

    }

    override fun onStart() {
        Log.i(TAG, "onStart: works!")
        super.onStart()
    }

    override fun onResume() {
        Log.i(TAG, "onResume: works!")
        super.onResume()
    }

    override fun onPause() {
        Log.i(TAG, "onPause: works!")
        super.onPause()
    }

    override fun onStop() {
        Log.i(TAG, "onStop: works!")
        super.onStop()
    }

    override fun onRestart() {
        Log.i(TAG, "onRestart: works!")
        super.onRestart()
    }

    fun passData(view: View){
        val selectedString = stringEt.text.toString()
        val selectedInt = intEt.text.toString().toInt()

        if(selectedString!!.isEmpty()){
            stringEt.error = "Field required!"
        }

        val intent = Intent(this, ActivityC::class.java)
        intent.putExtra("String", selectedString)
        intent.putExtra("Int", selectedInt)
        startActivityForResult(intent, Log.i(TAG, "Passing Int and String"))
    }

}