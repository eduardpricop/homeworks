package eduard.pricop.homework

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_b.*

class ActivityB : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_b)

        moveA.setOnClickListener {
            val intent: Intent = Intent(this, ActivityA::class.java)
            startActivity(intent)
        }

        moveC.setOnClickListener {
            val intent1: Intent = Intent(this, ActivityC::class.java)
            startActivity(intent1)
        }

        moveD.setOnClickListener {
            val intent2: Intent = Intent(this, ActivityD::class.java)
            startActivity(intent2)
        }

    }
}