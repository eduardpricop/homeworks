package eduard.pricop.homework

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_callback1.*

class Callback1 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_callback1)

        toParent.setOnClickListener {
            var intent = Intent()
            intent.putExtra("Data",etCallback.text.toString())
            setResult(0, intent)
            finish()
        }

    }
}