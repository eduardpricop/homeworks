package eduard.pricop.homework


import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_c.*


class ActivityC : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_c)

        ok_button.setOnClickListener {
            val intent: Intent = Intent(this, ActivityA::class.java)
            startActivityForResult(intent, Log.i("ok", "My choice was: OK!"))
        }

        cancel_button.setOnClickListener {
            val intent1: Intent = Intent(this, ActivityA::class.java)
            startActivityForResult(intent1, Log.i("cancel", "My choice was: CANCELED!"))
        }

        val intent = getIntent()
        val selectedString = intent.getStringExtra("String")
        val selectedInt = intent.getIntExtra("Int", 0)


        result.text = "The String is: " + selectedString +"\nThe Int is: "+ selectedInt


        backData.setOnClickListener {

            val intent2: Intent = Intent(this, ActivityA::class.java)
            startActivityForResult(intent2, Log.i("data", "My data was sent back: $selectedString $selectedInt"))
        }

    }

}