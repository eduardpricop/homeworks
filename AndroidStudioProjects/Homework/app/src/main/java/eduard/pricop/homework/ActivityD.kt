package eduard.pricop.homework

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_a.*
import kotlinx.android.synthetic.main.activity_d.*

class ActivityD : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_d)

        button2.setOnClickListener{
            val intent : Intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.endava.com/en"))
            startActivity(intent)
        }

        button3.setOnClickListener {
            val intent: Intent = Intent(this, Callback::class.java)
            startActivity(intent)
        }

    }
}