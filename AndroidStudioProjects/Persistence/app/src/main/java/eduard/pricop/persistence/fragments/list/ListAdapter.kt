package eduard.pricop.persistence.fragments.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import eduard.pricop.persistence.R
import eduard.pricop.persistence.model.Student
import kotlinx.android.synthetic.main.row.view.*

class ListAdapter : RecyclerView.Adapter<ListAdapter.MyViewHolder>() {

    private var studentList = emptyList<Student>()

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.row, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = studentList[position]
        holder.itemView.id_txt.text = currentItem.id.toString()
        holder.itemView.id_firstName.text = currentItem.firstName
        holder.itemView.id_lastName.text = currentItem.lastName
        holder.itemView.id_gpa.text = currentItem.gpa.toString()
        holder.itemView.id_birthday.text = currentItem.birthday.toString()
        holder.itemView.id_course.text = currentItem.course.title

        holder.itemView.edit.setOnClickListener {
            val action = ListFragmentDirections.actionListFragmentToUpdateFragment(currentItem)
            holder.itemView.findNavController().navigate(action)
        }
    }

    fun setData(student: List<Student>) {
        this.studentList = student
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return studentList.size
    }
}