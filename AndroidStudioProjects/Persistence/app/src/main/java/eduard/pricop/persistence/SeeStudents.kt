package eduard.pricop.persistence

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import eduard.pricop.persistence.ViewModel.StudentViewModel
import eduard.pricop.persistence.data.StudentDatabase
import eduard.pricop.persistence.fragments.list.SeeStudentsAdapter
import eduard.pricop.persistence.model.Course
import eduard.pricop.persistence.model.Student
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.fragment_start.*
import kotlinx.coroutines.launch

class SeeStudents : AppCompatActivity() {

    private lateinit var mStudentViewModel: StudentViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_see_students)


            val adapter = SeeStudentsAdapter()
            val recyclerView = recyclerview
            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(this)


            mStudentViewModel = ViewModelProvider(this).get(StudentViewModel::class.java)
            mStudentViewModel.readAllData.observe(this, Observer { student ->
                adapter.setData(student)
            })

        }
    }

