package eduard.pricop.persistence

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import eduard.pricop.persistence.ViewModel.StudentViewModel
import eduard.pricop.persistence.data.StudentDatabase
import eduard.pricop.persistence.fragments.list.ListAdapter
import eduard.pricop.persistence.model.Course
import eduard.pricop.persistence.model.Student
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.fragment_start.*
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dao = StudentDatabase.getDatabase(this).studentDao()

        val students = listOf(
            Student(1, "Eduard", "Pricop", 4.2, 22, Course(1, "English")),
            Student(2, "Radu", "Bob", 3.2, 10, Course(2, "Data Mining")),
            Student(3, "Carol", "Bobby", 2.3, 15, Course(3, "Cryptography")),
            Student(4, "Carla", "Bun", 4.0, 1, Course(4, "Maths")),
            Student(5, "Patricia", "Bunny", 3.7, 27, Course(5, "English"))
        )


        lifecycleScope.launch {
            students.forEach { dao.addStudent(it) }
            students.forEach { dao.englishStudents("English") }
        }


        setupActionBarWithNavController(findNavController(R.id.fragment))

    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.fragment)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

}