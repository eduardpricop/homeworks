package eduard.pricop.persistence.fragments.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import eduard.pricop.persistence.R
import eduard.pricop.persistence.model.Student
import kotlinx.android.synthetic.main.row.view.*


class SeeStudentsAdapter : RecyclerView.Adapter<SeeStudentsAdapter.MyViewHolder>() {

    private var studentList = emptyList<Student>()

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_no_edit, parent, false)
        return MyViewHolder(v)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = studentList[position]
        holder.itemView.id_txt.text = currentItem.id.toString()
        holder.itemView.id_firstName.text = currentItem.firstName
        holder.itemView.id_lastName.text = currentItem.lastName
        holder.itemView.id_gpa.text = currentItem.gpa.toString()
        holder.itemView.id_birthday.text = currentItem.birthday.toString()
        holder.itemView.id_course.text = currentItem.course.title

    }

    fun setData(student: List<Student>) {
        this.studentList = student
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return studentList.size
    }
}
