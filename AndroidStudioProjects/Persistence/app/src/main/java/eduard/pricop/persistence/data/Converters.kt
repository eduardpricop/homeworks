package eduard.pricop.persistence.data

import androidx.room.TypeConverter
import eduard.pricop.persistence.model.Date


class Converters {

    @TypeConverter
    fun fromTimestamp(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun dateToTimestamp(time: Long?): Date? {
        return time?.let { Date(it, time) }
    }


}