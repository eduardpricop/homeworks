package eduard.pricop.persistence.ViewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import eduard.pricop.persistence.data.StudentDatabase
import eduard.pricop.persistence.model.Course
import eduard.pricop.persistence.model.Student
import eduard.pricop.persistence.repository.StudentRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class StudentViewModel(application: Application) : AndroidViewModel(application) {

    val readAllData: LiveData<List<Student>>
    private val repository: StudentRepository

    init {
        val studentDao = StudentDatabase.getDatabase(application).studentDao()
        repository = StudentRepository(studentDao)
        readAllData = repository.readAllData

    }

    fun addStudent(student: Student) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.addStudent(student)
        }
    }

    fun incrementValue(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.incrementValue(id)
        }
    }

    fun englishStudents(course: String) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.englishStudents(course)
        }
    }

}