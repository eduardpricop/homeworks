package eduard.pricop.persistence.model

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import eduard.pricop.persistence.data.Converters
import kotlinx.android.parcel.Parcelize
import java.io.Serializable
import java.util.Date

@TypeConverters(Converters::class)
@Parcelize
@Entity(tableName = "Student_Table")
data class Student(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val firstName: String,
    val lastName: String,
    val gpa: Double,
    @field:TypeConverters(Converters::class) val birthday: Long,
    @Embedded
    val course: Course
): Parcelable

@Entity(tableName = "Course_Table")
data class Course(
    @PrimaryKey(autoGenerate = true)
    val course_id: Int = 0,
    val title: String
): Serializable

