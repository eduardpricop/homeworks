package eduard.pricop.persistence.repository

import androidx.lifecycle.LiveData
import eduard.pricop.persistence.data.StudentDao
import eduard.pricop.persistence.model.Course
import eduard.pricop.persistence.model.Student


class StudentRepository(private val studentDao: StudentDao) {

    val readAllData: LiveData<List<Student>> = studentDao.readAllData()

    suspend fun addStudent(student: Student) {
        studentDao.addStudent(student)
    }

    suspend fun incrementValue(id: Int) {
        studentDao.incrementValue(id)
    }

    fun englishStudents(course: String){
        studentDao.englishStudents(course)
    }

}