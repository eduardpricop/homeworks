package eduard.pricop.persistence.fragments.add

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import eduard.pricop.persistence.R
import eduard.pricop.persistence.ViewModel.StudentViewModel
import eduard.pricop.persistence.model.Course
import eduard.pricop.persistence.model.Student
import kotlinx.android.synthetic.main.fragment_add.*
import kotlinx.android.synthetic.main.fragment_add.view.*
import java.util.*


class AddFragment : Fragment() {

    private lateinit var mStudentViewModel: StudentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_add, container, false)

        mStudentViewModel = ViewModelProvider(this).get(StudentViewModel::class.java)

        view.add_button.setOnClickListener {
            insertData()
        }

        return view
    }

    private fun insertData() {
        val firstName = etFirstName.text.toString()
        val lastName = etLastName.text.toString()
        val gpa = etGPA.text
        val birthday = Date()
        val course = etCourse.text.toString()

        if (inputCheck(firstName, lastName, gpa, birthday, course)) {
            val student = Student(
                0,
                firstName,
                lastName,
                Integer.parseInt(gpa.toString()).toDouble(),
                birthday.time,
                Course(title = "English")
            )
            mStudentViewModel.addStudent(student)
            Toast.makeText(requireContext(), "Successfully added!", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_addFragment_to_listFragment)
        } else {
            Toast.makeText(requireContext(), "No empty fields allowed!", Toast.LENGTH_LONG).show()
        }

    }

    private fun inputCheck(
        firstName: String,
        lastName: String,
        gpa: Editable,
        birthday: Date,
        course: String
    ): Boolean {
        return !(TextUtils.isEmpty(firstName) && TextUtils.isEmpty(lastName) && gpa.isEmpty() && TextUtils.isEmpty(
            course
        ))
    }

}