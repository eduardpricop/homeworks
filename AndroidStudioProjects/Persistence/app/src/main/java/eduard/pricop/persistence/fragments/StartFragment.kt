package eduard.pricop.persistence.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import eduard.pricop.persistence.MainActivity
import eduard.pricop.persistence.R
import eduard.pricop.persistence.SeeStudents
import kotlinx.android.synthetic.main.fragment_start.view.*


class StartFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_start, container, false)

        view.floatingActionButton.setOnClickListener {
            findNavController().navigate(R.id.action_startFragment_to_addFragment)
        }

        view.see_students.setOnClickListener {
            val intent: Intent = Intent(requireContext(), SeeStudents::class.java)
            startActivity(intent)
        }

        view.see_english.setOnClickListener {
            val intent: Intent = Intent(requireContext(), MainActivity::class.java)
            startActivity(intent)
        }

        return view
    }


}