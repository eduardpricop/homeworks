package eduard.pricop.persistence.data

import androidx.lifecycle.LiveData
import androidx.room.*
import eduard.pricop.persistence.model.*


@Dao
interface StudentDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addStudent(student: Student)


    @Query("SELECT * FROM Student_Table ORDER BY id ASC")
    fun readAllData(): LiveData<List<Student>>

    @Transaction
    @Query("UPDATE Student_Table SET gpa = gpa + 1 WHERE id= :id")
    suspend fun incrementValue(id: Int)


    @Query("SELECT * FROM Student_Table WHERE title LIKE :course AND gpa >4")
    fun englishStudents(course: String): LiveData<List<Student>>


}
