package eduard.pricop.fragmenthomework.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import eduard.pricop.fragmenthomework.R
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*


class HomeFragment : Fragment(R.layout.fragment_home) {

    private val args: HomeFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        text_view_username.text = "Hello, ${args.user}"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)



        view.aboutButton.setOnClickListener {
            val action = HomeFragmentDirections.navigateToAboutFragment()
            findNavController().navigate(action)
        }

        view.profileButton.setOnClickListener {
            val action = HomeFragmentDirections.navigateToProfileFragment()
            findNavController().navigate(action)
        }



        return view
    }



    }

