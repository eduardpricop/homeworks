package eduard.pricop.fragmenthomework.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import eduard.pricop.fragmenthomework.R

class NewDialogFragment : DialogFragment() {

    val osVersion: String = Build.VERSION.RELEASE

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        AlertDialog.Builder(requireContext())
            .setMessage("App name is: " + getString(R.string.app_name) + "\nOS Version is: Android " +osVersion)
            .setPositiveButton(getString(R.string.positive_button)) {_, _ ->}
            .create()

        companion object {
            const val TAG = "NewDialogFragment"
        }



    }


