package eduard.pricop.fragmenthomework.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.airbnb.lottie.LottieAnimationView
import eduard.pricop.fragmenthomework.R

class EndavaFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_endava, container, false)

        val btn: LottieAnimationView = view.findViewById(R.id.dog)

        btn.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_endavaFragment_to_aboutFragment)
        }

        return view
    }

}