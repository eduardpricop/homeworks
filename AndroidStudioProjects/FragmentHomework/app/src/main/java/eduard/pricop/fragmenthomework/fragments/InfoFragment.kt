package eduard.pricop.fragmenthomework.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation
import eduard.pricop.fragmenthomework.R

class InfoFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_info, container, false)

        val btn: Button = view.findViewById(R.id.endava)
        btn.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.navigateToEndavaFragment)
        }

        return view
    }

}