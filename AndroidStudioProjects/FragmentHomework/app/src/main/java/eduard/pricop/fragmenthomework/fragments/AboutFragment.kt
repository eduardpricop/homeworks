package eduard.pricop.fragmenthomework.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import eduard.pricop.fragmenthomework.R


class AboutFragment : Fragment(R.layout.fragment_about) {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_about, container, false)

        NewDialogFragment().show(childFragmentManager, NewDialogFragment.TAG)

        return view
    }

}