package eduard.pricop.ui

import android.animation.Animator
import android.animation.ObjectAnimator
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.renderscript.ScriptGroup
import android.view.View
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import eduard.pricop.ui.adapter.RecyclerViewAdapter
import eduard.pricop.ui.model.Model
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.model_artists.*
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {

    private var layoutManager: RecyclerView.LayoutManager? = null
    private var adapter: RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

            layoutManager = LinearLayoutManager(this)
            list.layoutManager = layoutManager

            adapter = RecyclerViewAdapter(this, Model.Supplier.artists)
            list.adapter = adapter

       var lottie = findViewById<LottieAnimationView>(R.id.bee)
        val fadeOutAnim: Animation = AnimationUtils.loadAnimation(this, R.anim.fade_out)
        lottie.startAnimation(fadeOutAnim)


    }

    }


