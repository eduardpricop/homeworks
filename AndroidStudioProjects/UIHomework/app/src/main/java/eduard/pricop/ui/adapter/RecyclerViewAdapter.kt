package eduard.pricop.ui.adapter

import android.content.Context
import android.content.DialogInterface
import android.media.Image
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.makeramen.roundedimageview.RoundedImageView
import eduard.pricop.ui.R
import eduard.pricop.ui.model.Model
import kotlinx.android.synthetic.main.model_artists.*
import kotlinx.android.synthetic.main.model_artists.view.*
import kotlin.coroutines.coroutineContext

class RecyclerViewAdapter(val context: Context, val artists: List<Model>) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>(){


    inner class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
       fun setData(model: Model?, position: Int){
           itemView.pop_name.text = model!!.title
           itemView.pop_des.text = model!!.description

           Glide.with(itemView.context)
               .load(model.imageUrl)
               .into(imagine)

            deleteM.setOnClickListener {
                val builder = AlertDialog.Builder(context)
                builder.setMessage("By pressing YES you will delete the item, otherwise nothing will happen.")
                builder.setPositiveButton("Yes", { dialogInterface: DialogInterface, i: Int ->
                    removeItem(model)})
                builder.setNegativeButton("No", { dialogInterface: DialogInterface, i: Int -> })
                builder.show()
            }

           cardview.setOnLongClickListener{
               Toast.makeText(context, "${itemView.pop_des.text}", Toast.LENGTH_LONG).show()
               true
           }
           cardview.setOnClickListener {
               val snackbar = Snackbar.make(itemView, "${itemView.pop_name.text}", Snackbar.LENGTH_INDEFINITE)
               snackbar.setAction("Enough!", View.OnClickListener {
                   snackbar.dismiss()
               })
               snackbar.show()
           }

       }


    fun removeItem(model: Model?){
        val currentposition = artists.indexOf(model)
        artists.drop(currentposition)
        notifyItemRemoved(currentposition)
    }
        val deleteM = itemView.findViewById<ImageView>(R.id.delete)
        val imagine = itemView.findViewById<ImageView>(R.id.pop_img)
        val cardview = itemView.findViewById<ConstraintLayout>(R.id.allview)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.model_artists, parent, false)
        return ViewHolder(v)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = artists[position]
        holder.setData(model, position)

        }


    override fun getItemCount(): Int {
        return artists.size
    }



}



