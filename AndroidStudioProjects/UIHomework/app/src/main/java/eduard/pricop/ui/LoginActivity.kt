package eduard.pricop.ui

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.ColorRes
import kotlinx.android.synthetic.main.activity_login.*



class LoginActivity : AppCompatActivity() {

    lateinit var inputUsername: EditText
    lateinit var inputPassword: EditText
    lateinit var actionButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setSupportActionBar(toolbar)

        inputUsername = findViewById(R.id.etUsername)
        inputPassword = findViewById(R.id.etPassword)
        actionButton = findViewById(R.id.logButton)

        inputUsername.addTextChangedListener(textWatcher)
        inputPassword.addTextChangedListener(textWatcher)

        buttonColor()

        logButton.setOnClickListener {
            val intent: Intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        registerText.setOnClickListener {
            val intent1: Intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent1)
        }

    }

    private fun buttonColor() {
        if(logButton!!.isEnabled){
            logButton.setTextColor(Color.BLACK)
        }
    }

    val textWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            val username: String = inputUsername.text.toString().trim()
            val password: String = inputPassword.text.toString().trim()

            actionButton.isEnabled = password.length > 0 && username.length > 0
        }


        override fun afterTextChanged(s: Editable?) {

        }

    }
}