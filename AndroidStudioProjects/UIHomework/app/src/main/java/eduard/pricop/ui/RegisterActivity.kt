package eduard.pricop.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.toolbar.*

class RegisterActivity : AppCompatActivity() {

    lateinit var inputUsername: EditText
    lateinit var inputPassword: EditText
    lateinit var actionButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        inputUsername = findViewById(R.id.etUsername)
        inputPassword = findViewById(R.id.etPassword)
        actionButton = findViewById(R.id.regButton)

        inputUsername.addTextChangedListener(textWatcher)
        inputPassword.addTextChangedListener(textWatcher)

        backArrow.setOnClickListener{
            val intent: Intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        regButton.setOnClickListener {
            validatePassword()
        }

        registerText.setOnClickListener {
            val intent: Intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        toolbarTitle.text = "RegisterActivity"


    }

    val textWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            val username: String = inputUsername.text.toString().trim()
            val password: String = inputPassword.text.toString().trim()

            actionButton.isEnabled = password.length > 0 && username.length > 0
        }

        override fun afterTextChanged(s: Editable?) {

        }

    }

    private fun validatePassword() {

        val password: String = etPassword.text.toString().trim()
        val cpassword: String = cPassword.text.toString().trim()
        if(password.equals(cpassword)){
            val intent: Intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }else
        {
            confirmPassword.error = "Passwords must be the same."
        }
    }


}