package eduard.pricop.ui.model

data class Model(var title: String, var description: String, var imageUrl: String)  {

    object Supplier {
        val artists = listOf<Model>(
            Model("Chester Bennington", "Chester was an American singer, songwriter, and occasional actor. He served as the lead vocalist of the bands Linkin Park, Grey Daze, Dead by Sunrise, and Stone Temple Pilots.", "https://www.antena3.ro/thumbs/big3/2017/07/20/a-murit-chester-bennington-solistul-trupei-linkin-park-s-a-sinucis-465585.jpg" ),
            Model("Chester Bennington", "Chester was an American singer, songwriter, and occasional actor. He served as the lead vocalist of the bands Linkin Park, Grey Daze, Dead by Sunrise, and Stone Temple Pilots.", "https://www.antena3.ro/thumbs/big3/2017/07/20/a-murit-chester-bennington-solistul-trupei-linkin-park-s-a-sinucis-465585.jpg"),
            Model("Chester Bennington", "Chester was an American singer, songwriter, and occasional actor. He served as the lead vocalist of the bands Linkin Park, Grey Daze, Dead by Sunrise, and Stone Temple Pilots.", "https://www.antena3.ro/thumbs/big3/2017/07/20/a-murit-chester-bennington-solistul-trupei-linkin-park-s-a-sinucis-465585.jpg"),
            Model("Chester Bennington", "Chester was an American singer, songwriter, and occasional actor. He served as the lead vocalist of the bands Linkin Park, Grey Daze, Dead by Sunrise, and Stone Temple Pilots.", "https://www.antena3.ro/thumbs/big3/2017/07/20/a-murit-chester-bennington-solistul-trupei-linkin-park-s-a-sinucis-465585.jpg"),
            Model("Chester Bennington", "Chester was an American singer, songwriter, and occasional actor. He served as the lead vocalist of the bands Linkin Park, Grey Daze, Dead by Sunrise, and Stone Temple Pilots.", "https://www.antena3.ro/thumbs/big3/2017/07/20/a-murit-chester-bennington-solistul-trupei-linkin-park-s-a-sinucis-465585.jpg"),
            Model("Chester Bennington", "Chester was an American singer, songwriter, and occasional actor. He served as the lead vocalist of the bands Linkin Park, Grey Daze, Dead by Sunrise, and Stone Temple Pilots.", "https://www.antena3.ro/thumbs/big3/2017/07/20/a-murit-chester-bennington-solistul-trupei-linkin-park-s-a-sinucis-465585.jpg"),
            Model("Chester Bennington", "Chester was an American singer, songwriter, and occasional actor. He served as the lead vocalist of the bands Linkin Park, Grey Daze, Dead by Sunrise, and Stone Temple Pilots.", "https://www.antena3.ro/thumbs/big3/2017/07/20/a-murit-chester-bennington-solistul-trupei-linkin-park-s-a-sinucis-465585.jpg")
        )
    }

}